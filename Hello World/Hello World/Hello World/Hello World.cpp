// These are windows specific header files that load all the "pre-compiled" headers for use in the program.
// Standard libraries are rarely modified, and thus are pre-compiled to avoid recompilation everytime.

//DOCUMENTATION
/*
----------
Section 1
----------
	The input output stream library. This library contains the "cout" and "cin" objects that handle the 
	input and output from the windows "console" (shell)

----------
Section 2
----------

----------
Section 3
----------

	This is the main entry point of the program. The reason we are not using int main() is because,
	windows is internally using it to extract the arguments (argc,argv) from the console to
	this program. It is purely an OS specific modification.
	For us, _tmain is exactly same as main()

	Note : The arguments are the parameters sent to the program when this program is run from windows
	shell.

----------
Section 4
----------
	Output the customary Hello World line to the console.
	User will be able to see the output on his console.
	std -> Represents the "std" namespace.
	A namespace is a collection of definitions, functions, classes, etc
	Namespaces were introduced for dealing with large software projects 
	where uniquely naming every class/type used by the project is very cumbersome.
	As far as the compiler is concerned, it treats the same class in different 
	namespaces as different classes.
	Internally, the compiler generates unique names for classes under one namespace.
	For example std::cout could be converted to std_cout / cout_std, etc. This
	depends on the compiler used for compiling.

----------
Section 5
---------
	A string variable is declared and 

----------
Section 6
----------
	 The return status of the program.
	 0 implies everything went smoothly with the program.
	 This value is picked up by the OS.

*/
//DOCUMENTATION


#include "stdafx.h"

// Section 1
#include <iostream>
#include <string>

// Section 2
using namespace std;

// Section 3
int _tmain(int argc, _TCHAR* argv[])
{
	// Section 4
	std::cout<<"Hello World";

	// Section 5
	string name;
	name = "yahoo";
	cout<<"The name is :"<<name<<" .";
	cin;
	
	// Section 6
	return 0;
}

